import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)


#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',"")})
        return data


#dimensi fact users
def join_infoUC((k,v)):
    return itertools.product(v['user_id'], v['channel_id'])

def merge_dictUC(user_channel):
      (user, channel) = user_channel
      a_dict = user
      a_dict.update(channel)
      return a_dict

#dimensi fact doctors
class DenormalizedFDFn(beam.DoFn):
    def process (self,element):
        data = []
        if element.get("educations"):
            for education in element['educations']:
                data.append((education['name'],
                            {'doctor_id':str(element['_id']),
                            'year':int(education['year_of_graduation'])}))
        else:
             data.append((None, {'doctor_id':str(element['_id']), 'year':None}))
        return data

def join_infoFD((k,v)):
    doctors = []
    if (v.get('university', None)) is not None:
        data=itertools.product(v['doctor'], v['university'])
    else:
        data=itertools.product(v['doctor'], '')

    for element in data:
        if (element[1] == ' '):university_id = None
        else: university_id = element[1]

        doctors.append({'id': str(uuid.uuid4()),'doctor_id':str(element[0]['doctor_id']),
                        'university_id':university_id,
                        'graduate_year_id':element[0]['year']})
    return doctors

class TransformDoctorsFn(beam.DoFn):
    def process (self, element):
        print (element)

#dimensi fact_magazine
class DenormalizeFMFn(beam.DoFn):
    def process(self, element):
        magazines_relationships = element.get('magazine_relationships')
        magazines = []
        if magazines_relationships is not None:
            if (len(magazines_relationships) > 0):
                for term_taxonomy_ids in magazines_relationships:
                    for data in [term_taxonomy_ids]:
                        magazines.append({'id':str(uuid.uuid4()),
                                          'article_id':element['post_id'],
                                          'term_taxonomy_id':data['term_taxonomy_id'],
                                          'created_at':element['created_at']})
        else:
                    magazines.append({'id':str(uuid.uuid4()),
                              'article_id':element['post_id'],
                              'term_taxonomy_id':None,
                              'created_at':element['created_at']})
        return magazines

class GetTermMagFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        term_taxonomies = db.term_taxonomies
        term_list = []
        if element.get('term_taxonomy_id', None) is not None:
            for term in term_taxonomies.find({'_id':ObjectId(element['term_taxonomy_id'])}):
                term_list.append({'id':element['id'],
                                  'article_id':element['article_id'],
                                  'term_id':str(term['term_id']),
                                  'created_at':element['created_at']})
        else:
                term_list.append({'id':element['id'],
                      'article_id':element['article_id'],
                      'term_id':None,
                      'created_at':element['created_at']})
        return term_list

# pipeline_options = PipelineOptions()
# google_cloud_options = pipeline_options.view_as(GoogleCloudOptions)
# google_cloud_options.project = 'plenary-justice-151004'
# pipeline_options.view_as(StandardOptions).runner = 'DirectRunner'

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])







 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={"_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )

    with beam.Pipeline(options = options) as pipeline_fact_magazine:
      (pipeline_fact_magazine
        |'ReadMagazine' >> ReadFromMongo(connection_string, 'alomobile', 'magazines', query={}, fields=['post_id', 'created_at', 'magazine_relationships.term_taxonomy_id'])
        | 'Denormalize' >> beam.ParDo(DenormalizeFMFn())
        | 'GetTerm' >> beam.ParDo(GetTermMagFn())
        | 'FormatData' >> beam.Map(lambda x:{'id':x['id'],
                                           'article_id':str(x['article_id']),
                                           'term_id': str(x['term_id']),
                                           'created_at':str(x['created_at']),
                                           'day_id': x['created_at'].isoweekday(),
                                           'date_id':x['created_at'].day,
                                           'month_id':x['created_at'].month,
                                           'year_id':x['created_at'].year,
                                           'hour_id':x['created_at'].hour,
                                           'minute_id':x['created_at'].minute,
                                           'quarter_id':int(math.ceil(x['created_at'].month/3.))})
        # | 'debug' >> beam.ParDo (debug_function)
        |'WriteFactMagazineToBQ' >> beam.io.Write(
                 beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_magazine'),
                 schema='id:STRING, term_id:STRING, article_id:STRING, created_at:DATETIME, day_id:INTEGER, date_id:INTEGER, month_id:INTEGER, quarter_id:INTEGER, year_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
                 create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                 write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    )

    with beam.Pipeline(options = options) as pipeline_fact_doctors:
      universities = (pipeline_fact_doctors
                    |'readdatauniv' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT * FROM alowarehouse_alodoktermobile.universities', use_standard_sql=True))
                    |'Extract text column' >>  beam.Map(lambda row: (row['university'],row['id']))
                    )
      doctor = (pipeline_fact_doctors
                |'ReadDoctor' >> ReadFromMongo(connection_string, 'alomobile', 'users', query={'_type':'Core::Doctor'},
                                                         fields=['educations.name','educations.year_of_graduation'])
                |'DenormalizedFD' >> beam.ParDo (DenormalizedFDFn())
                )
      result = ({'doctor':doctor,'universities':universities} | 'result:doc&univ' >> beam.CoGroupByKey()
                                                            | 'joinFD' >> beam.FlatMap(join_infoFD)
                                                            #|'DebugFD' >> beam.Map (debug_function)
                  | 'writeFDToBQ' >> beam.io.Write(
                                  beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_doctors'),
                                  schema='id:STRING, doctor_id:STRING,university_id:STRING, graduate_year_id:INTEGER',
                                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                                  write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_fact_users:
      registration_channel = (pipeline_fact_users
        |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
        |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
                        )
      user = (pipeline_fact_users
        |'Readusers' >> ReadFromMongo(connection_string, 'alomobile', 'users',
                                         query={'_type':'Core::User'},
                                         fields=['_id', 'provider', 'created_at'])
        | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
            )
      result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
                |'joinUC'>> beam.FlatMap(join_infoUC)
                |'merge_dictUC' >> beam.Map(merge_dictUC)
                |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
                                                    'user_id':str(x['user_id']),
                                                    'channel_id':x['channel_id'],
                                                    'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
                                                    'year_id':x['created_at'].year,
                                                    'month_id':x['created_at'].month,
                                                    'date_id':x['created_at'].day,
                                                    'hour_id':x['created_at'].hour,
                                                    'minute_id':x['created_at'].minute})
#               |'Debug_fact_users' >> beam.Map(debug_function)
                | 'writefactusersToBQ' >> beam.io.Write(
                              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users'),
                              schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
                              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
         )
