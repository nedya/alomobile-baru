import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

#dfact questions
class if_meta_question(beam.DoFn):
    def process(self, element):
        data = []
        if element.get('time_picked_up') is not None:
            if element['_type'] == "Core::Question" or element['_type'] == "Core::PreQuestion":
                data.append({'topic':element['topic'],
                                  'question_id':str(element['_id']),
                                  'meta_question_id':str(element.get('meta_question_id',"")),
                                  'user_id':str(element['user_id']),
                                  'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                                  'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                                  'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                  'intent_id':str(element.get('intent_id',"")),
                                  'sub_intent_id':str(element.get('sub_intent_id',"")),
                                  'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
                                  'journal_id':str(element.get('journal_id')),
                                  'date_picked_up':str(element['time_picked_up'])[0:10]})

            elif element['_type'] == "Core::MetaQuestion":
                data.append({'topic':element['topic'],
                                  'question_id':None,
                                  'meta_question_id':str(element['_id']),
                                  'user_id':str(element['user_id']),
                                  'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                                  'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                                  'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                  'intent_id':str(element.get('intent_id',"")),
                                  'sub_intent_id':str(element.get('sub_intent_id',"")),
                                  'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
                                  'journal_id':str(element.get('journal_id')),
                                  'date_picked_up':str(element['time_picked_up'])[0:10]})
        return data

class GetTermsFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        terms = []
        term_query = db.terms.find({'name':element['topic']})
        if element.get('time_picked_up') is not None:
            for term in term_query:
                if element.get('journal_id', None) is not None:
                    terms.append({'topic_id':str(term['_id']),
                                  'question_id':element['question_id'],
                                  'meta_question_id':element['meta_question_id'],
                                  'user_id':element['user_id'],
                                  'picked_up_by_id':element['picked_up_by_id'],
                                  'created_at':element['created_at'],
                                  'updated_at':element['updated_at'],
                                  'intent_id':element['intent_id'],
                                  'sub_intent_id':element['sub_intent_id'],
                                  'time_picked_up':element['time_picked_up'],
                                  'journal_id':element['journal_id'],
                                  'date_picked_up':element['date_picked_up']})
                elif element.get('journal_id', None) is None:
                    terms.append({'topic_id':str(term['_id']),
                                  'question_id':element['question_id'],
                                  'meta_question_id':element['meta_question_id'],
                                  'user_id':element['user_id'],
                                  'picked_up_by_id':element['picked_up_by_id'],
                                  'created_at':element['created_at'],
                                  'updated_at':element['updated_at'],
                                  'intent_id':element['intent_id'],
                                  'sub_intent_id':element['sub_intent_id'],
                                  'time_picked_up':element['time_picked_up'],
                                  'journal_id':None,
                                  'date_picked_up':element['date_picked_up']})
        return terms

class GetConclusionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        conclusions = []
        conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
        for conclusion in conclusion_query:
            conclusions.append({'conclusion_id':str(conclusion['_id']),
                                'topic_id':element['topic_id'],
                                'question_id':element['question_id'],
                                'meta_question_id':element['meta_question_id'],
                                'user_id':element['user_id'],
                                'picked_up_by_id':element['picked_up_by_id'],
                                'created_at':element['created_at'],
                                'updated_at':element['updated_at'],
                                'intent_id':element['intent_id'],
                                'sub_intent_id':element['sub_intent_id'],
                                'time_picked_up':element['time_picked_up'],
                                'journal_id':element['journal_id'],
                                'date_picked_up':element['date_picked_up']})
        return conclusions

class GetPaymentFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        journals = []
        if element['journal_id'] is not None and element['journal_id'] != 'None':
            journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
            for journal in journal_query:
                journals.append({'id':str(uuid.uuid4()),
                                 'conclusion_id':str(conclusion['_id']),
                                 'topic_id':element['topic_id'],
                                 'question_id':element['question_id'],
                                 'meta_question_id':element['meta_question_id'],
                                 'user_id':element['user_id'],
                                 'picked_up_by_id':element['picked_up_by_id'],
                                 'created_at':element['created_at'],
                                 'updated_at':element['updated_at'],
                                 'intent_id':element['intent_id'],
                                 'sub_intent_id':element['sub_intent_id'],
                                 'time_picked_up':element['time_picked_up'],
                                 'journal_id':element['journal_id'],
                                 'payment_method_id':str(journal['payment_method_id']),
                                 'date_picked_up':element['date_picked_up']})
        elif element['journal_id'] is None or element['journal_id'] == 'None':
            journals.append({'id':str(uuid.uuid4()),
                             'conclusion_id':str(element['conclusion_id']),
                             'topic_id':element['topic_id'],
                             'question_id':element['question_id'],
                             'meta_question_id':element['meta_question_id'],
                             'user_id':element['user_id'],
                             'picked_up_by_id':element['picked_up_by_id'],
                             'created_at':element['created_at'],
                             'updated_at':element['updated_at'],
                             'intent_id':element['intent_id'],
                             'sub_intent_id':element['sub_intent_id'],
                             'time_picked_up':element['time_picked_up'],
                             'journal_id':None,
                             'payment_method_id':None,
                             'date_picked_up':element['date_picked_up']})
        return journals

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

    with beam.Pipeline(options = options) as pipeline_fact_questions5:
        (pipeline_fact_questions5
           | 'read_questionsQ5' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 10, 1)}} ,
                                fields=['meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ5' >> beam.ParDo(if_meta_question())
           | 'get_termsQ5' >> beam.ParDo(GetTermsFn())
           | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ5' >> beam.ParDo(GetPaymentFn())
    #        | 'DebugQ5' >> beam.Map(debug_function)
           | 'writeFQ5ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions5'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
           )

    with beam.Pipeline(options = options) as pipeline_fact_questions4:
        (pipeline_fact_questions4
           | 'read_questionsQ4' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 7, 1),'$lte':datetime(2018, 9, 30)} } ,
                                fields=['meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ4' >> beam.ParDo(if_meta_question())
           | 'get_termsQ4' >> beam.ParDo(GetTermsFn())
           | 'get_conclusionsQ4' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ4' >> beam.ParDo(GetPaymentFn())
    #        | 'DebugQ4' >> beam.Map(debug_function)
           | 'writeFQ4ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions4'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
           )

    with beam.Pipeline(options = options) as pipeline_fact_questions3:
        (pipeline_fact_questions3
           | 'read_questionsQ3' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 4, 1),'$lte':datetime(2018, 6, 30)} } ,
                                fields=['meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ3' >> beam.ParDo(if_meta_question())
           | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
           | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    #        | 'DebugQ3' >> beam.Map(debug_function)
           | 'writeFQ3ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions3'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
          )

    with beam.Pipeline(options = options) as pipeline_fact_questions2:
        (pipeline_fact_questions2
           | 'read_questionsQ2' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 1, 1),'$lte':datetime(2018, 3, 30)} } ,
                                fields=['meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ2' >> beam.ParDo(if_meta_question())
           | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
           | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    #        | 'DebugQ2' >> beam.Map(debug_function)
           | 'writeFQ2ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions2'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
           )

    with beam.Pipeline(options = options) as pipeline_fact_questions1:
        (pipeline_fact_questions1
           | 'read_questionsQ1' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2016, 3, 11),'$lte':datetime(2017, 12, 31)} } ,
                                fields=['meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ1' >> beam.ParDo(if_meta_question())
           | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
           | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    #        | 'DebugQ1' >> beam.Map(debug_function)
           | 'writeFQ1ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions1'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )
