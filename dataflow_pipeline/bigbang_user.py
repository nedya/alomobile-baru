import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)


#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',"")})
        return data

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

    with beam.Pipeline(options = options) as pipeline_users5:
        (pipeline_users5
         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,01, 1)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users5'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )

    with beam.Pipeline(options = options) as pipeline_users4:
        (pipeline_users4
         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,01, 1), '$lte':datetime(2017, 12, 31)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users4'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )

    with beam.Pipeline(options = options) as pipeline_users3:
        (pipeline_users3
         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2016,01, 1), '$lte':datetime(2016, 12, 31)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users3'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )

    with beam.Pipeline(options = options) as pipeline_users2:
        (pipeline_users2
         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2015,01, 1), '$lte':datetime(2015, 12, 31)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users2'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )

    with beam.Pipeline(options = options) as pipeline_users1:
        (pipeline_users1
         | 'ReadUsers'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$lte':datetime(2014,12,31)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users1'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )
